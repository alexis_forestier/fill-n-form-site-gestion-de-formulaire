# FILL N FORM - site gestion de formulaire

Projet tutoré (version originale et finale)

En collaboration avec @durairajuprawinraj01, @elenormand, @hedi_zair

![](/img/Logo.png)

## Introduction

Dans le cadre des projets tutorés du DUT informatique, supervisés par M. TELLEZ, nous étions en charge de la création d’un site de génération et de gestion de formulaires. L’objectif principal de ce site est la possibilité pour chaque utilisateur de créer un formulaire et de le publier. Chaque utilisateur pouvant, ou non, faire partie d’un groupe peut répondre à des formulaires partagés avec lui.

## Les requis
- PHP version 7+ ([download](https://www.php.net/downloads.php))

## Les technologies utilisés

- HTML 5
- CSS 3
- JavaScript
    - [jQuery UI](https://jqueryui.com/)
    - [HighCharts](https://www.highcharts.com/)
    - [BootStrap](https://getbootstrap.com/) + [Pooper](https://popper.js.org/)
- PHP 7
- SQLite

## Modèle de données

![](/img/SchemaDB.png)

## Installation

1. Cloner le projet
2. Télécharger la base de données
3. Placer la base de données dans le dossier principal
4. Aller dans le dossier projet et ouvrer le terminal
5. Entrer la command : `php -S localhost:80`
6. Dans un navigateur, aller à l'adresse suivante : `localhost:80`

Vous devriez arriver sur cette page :

![](/img/loginPage.png)

## Données du Git d'origine

Projet Git original est disponible sur [GitLab Forge](https://forge.univ-lyon1.fr/).

Le projet Git original est privé.

| Commits | 285 |
|---|---|
| Branches | 10 |

